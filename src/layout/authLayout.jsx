import { Outlet } from "react-router-dom";
import Footer from "../components/footer/Footer";
import AuthHeader from "../components/header/AuthHeader";

const AuthLayout = () => {
	return (
		<div className="grid grid-cols-1 md:grid-cols-8 lg:grid-cols-12 w-full h-screen">
			<div className="col-span-1 md:col-span-8 lg:col-span-12">
				<AuthHeader />
			</div>
			<main className="col-span-1 md:col-span-8 lg:col-span-12">
				<Outlet />
			</main>
			<div className="col-span-1 md:col-span-8 lg:col-span-12">
				<Footer />
			</div>
		</div>
	);
};

export default AuthLayout;
