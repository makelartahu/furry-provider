const Footer = () => {
	return (
		<footer className="bg-[#20307d] py-8">
			<div className="container mx-auto flex flex-col md:flex-row justify-between items-center">
				<div className="text-white mb-4 md:mb-0">
					<p>&copy; 2024 Furry Provider. All rights reserved.</p>
				</div>
				<div className="flex">
					<a href="#facebook" className="text-white hover:text-blue-300 mr-4">
						<i className="fab fa-facebook fa-2x"></i>
					</a>
					<a href="#twitter" className="text-white hover:text-blue-300 mr-4">
						<i className="fab fa-twitter fa-2x"></i>
					</a>
					<a href="#instagram" className="text-white hover:text-blue-300">
						<i className="fab fa-instagram fa-2x"></i>
					</a>
				</div>
			</div>
		</footer>
	);
};

export default Footer;
