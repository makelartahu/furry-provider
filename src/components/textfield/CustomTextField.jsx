import TextField from "@mui/material/TextField";
import PropTypes from "prop-types";

const CustomTextField = ({ label, placeholder, name, type, getInput, ...props }) => {
	const handleInput = (e) => {
		getInput(e);
	};

	return (
		<TextField
			label={label}
			placeholder={placeholder}
			name={name}
			type={type}
			fullWidth
			{...props}
			onChange={handleInput}
		/>
	);
};

CustomTextField.propTypes = {
	label: PropTypes.string.isRequired,
	placeholder: PropTypes.string,
	name: PropTypes.string.isRequired,
	type: PropTypes.string,
	getInput: PropTypes.func,
};

export default CustomTextField;
