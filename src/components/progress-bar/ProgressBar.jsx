// Progress.jsx
import React from "react";

import PropTypes from "prop-types";

const Progress = ({ currentStep }) => {
	const steps = ["Registrasi", "Verifikasi", "Inprogress Pasang", "Selesai"];

	// Map the currentStep to the progress bar's steps
	let progressBarStep;
	switch (currentStep) {
		case 1:
			progressBarStep = 0;
			break;
		case 2:
		case 3:
		case 4:
			progressBarStep = 1;
			break;
		case 5:
		case 6:
			progressBarStep = 2;
			break;
		case 7:
			progressBarStep = 3;
			break;
		default:
			progressBarStep = 0;
			break;
	}

	const getBorderRadius = (index) => {
		if (index === 0) {
			return "rounded-l-md";
		} else if (index === steps.length - 1) {
			return "rounded-r-md";
		} else {
			return "";
		}
	};

	return (
		<div className="flex justify-center items-center">
			{steps.map((step, index) => (
				<div
					key={step}
					className={`${getBorderRadius(index)}  border-2 border-black p-4 font-sans text-xl font-extrabold ${
						index <= progressBarStep ? "bg-green-500 text-black border-green-500" : "bg-white text-black border-black"
					}`}>
					{step}
				</div>
			))}
		</div>
	);
};

Progress.propTypes = {
	currentStep: PropTypes.number,
};

export default Progress;
