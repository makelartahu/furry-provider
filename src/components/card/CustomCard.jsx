import React from "react";
import PropTypes from "prop-types";

const CustomCard = ({ text, textColor, IconComponent, flexDirection, size, shadow, elevation, iconSize }) => (
	<div
		className={`bg-slate-300 font-sans shadow-${shadow} ${elevation} sm:w-full md:w-[370px] lg:w-[${size.width}] lg:h-[${size.height}]`}>
		<div
			className={`flex ${flexDirection === "row" ? "items-center" : "flex-col"} justify-between p-4 mt-4 md:flex-row`}>
			<div className="mb-4 md:mb-0">
				<h4 className="text-xl font-bold w-44">
					<span style={{ color: textColor }}>{text}</span>
				</h4>
				<p className="text-sm mt-4 text-[#20307d] whitespace-normal w-48">
					Lorem ipsum dolor, sit amet consectetur adipisicing elit
				</p>
			</div>
			<div className="w-[90px] h-[90px] bg-blue-500 rounded-full flex justify-center items-center">
				<IconComponent style={{ fontSize: iconSize, color: "white" }} />
			</div>
		</div>
	</div>
);

CustomCard.propTypes = {
	text: PropTypes.string.isRequired,
	textColor: PropTypes.string.isRequired,
	IconComponent: PropTypes.elementType.isRequired,
	flexDirection: PropTypes.string.isRequired,
	size: PropTypes.shape({
		height: PropTypes.string.isRequired,
		width: PropTypes.string.isRequired,
	}).isRequired,
	shadow: PropTypes.string.isRequired,
	elevation: PropTypes.string.isRequired,
	iconSize: PropTypes.number.isRequired,
};

export default CustomCard;
