import React from "react";
import PropTypes from "prop-types";

const PackageCard = ({ title, price, speed, devices }) => (
	<div className="bg-white rounded-3xl shadow-xl p-8 flex flex-col items-center w-full md:w-[400px] mb-8 md:mb-0">
		<span className="text-[24px] md:text-[30px] text-slate-500 font-bold mt-6 md:mt-10">{title}</span>
		<h1 className="text-[48px] md:text-[60px] font-bold mt-10 md:mt-14">{price}</h1>
		<h2 className="text-[24px] md:text-[30px] text-slate-500 font-bold mt-10 md:mt-14">{speed}</h2>
		<h3 className="text-[24px] md:text-[30px] text-slate-500 font-bold mt-10 md:mt-14">{devices}</h3>
		<button className="border-4 border-blue-500 text-blue-500 font-bold px-4 py-2 rounded h-12 md:h-14 w-48 md:w-52 mt-10 md:mt-14 hover:bg-[#37b1e4] hover:text-white">
			Chose Package
		</button>
	</div>
);

PackageCard.propTypes = {
	title: PropTypes.string,
	price: PropTypes.string,
	speed: PropTypes.string,
	devices: PropTypes.string,
};

export default PackageCard;
