import React, { useState } from "react";
import { Link as RouterLink } from "react-router-dom";
import MenuIcon from "@mui/icons-material/Menu";
import CloseIcon from "@mui/icons-material/Close";

const OrderHeader = () => {
	const [isOpen, setIsOpen] = useState(false);

	const toggleMenu = () => {
		setIsOpen(!isOpen);
	};

	return (
		<header className="bg-gradient-to-r from-[#20307d] to-[#37b1e4] p-4">
			<nav className="mx-auto flex flex-col md:flex-row items-center justify-between">
				<div className="flex justify-between items-center w-full md:w-auto">
					<img src={"/assets/image/logo.png"} alt="Furry Provider Logo" className="h-14 w-14 mr-4" />
					<h1 className="text-white text-2xl font-bold">Furry Provider</h1>
					<div className="md:hidden text-white">
						{isOpen ? <CloseIcon onClick={toggleMenu} /> : <MenuIcon onClick={toggleMenu} />}
					</div>
				</div>

				<div
					className={`flex flex-col md:flex-row justify-center items-center ${isOpen ? "block" : "hidden"} md:block`}>
					<RouterLink to="/auth/login" className="text-white hover:text-blue-300 mx-4 block py-2 md:inline-block">
						Logout
					</RouterLink>
				</div>
			</nav>
		</header>
	);
};

export default OrderHeader;
