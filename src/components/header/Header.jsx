import React, { useState } from "react";
import { Link as ScrollLink } from "react-scroll";
import { Link as RouterLink } from "react-router-dom";
import MenuIcon from "@mui/icons-material/Menu";
import CloseIcon from "@mui/icons-material/Close";

const Header = () => {
	const [isOpen, setIsOpen] = useState(false);

	const toggleMenu = () => {
		setIsOpen(!isOpen);
	};

	return (
		<header className="bg-gradient-to-r from-[#20307d] to-[#37b1e4] p-4">
			<nav className="mx-auto grid grid-cols-1 md:grid-cols-8 lg:grid-cols-12 items-center">
				<div className="md:col-start-1 flex justify-between items-center">
					<img src={"public/assets/image/logo.png"} alt="Furry Provider Logo" className="h-14 w-14 mr-4" />
					<h1 className="text-white text-2xl font-bold">Furry Provider</h1>
					<div className="md:hidden text-white">
						{isOpen ? <CloseIcon onClick={toggleMenu} /> : <MenuIcon onClick={toggleMenu} />}
					</div>
				</div>
				<div
					className={`flex flex-col md:flex-row justify-center items-center md:col-start-4 md:col-span-6 mx-auto ${
						isOpen ? "block" : "hidden"
					} md:block`}>
					<ScrollLink to="" smooth="true" className="text-white hover:text-blue-300 mx-4 block py-2 md:inline-block">
						Home
					</ScrollLink>
					<ScrollLink
						to="about"
						smooth="true"
						className="text-white hover:text-blue-300 mx-4 block py-2 md:inline-block">
						About
					</ScrollLink>
					<ScrollLink
						to="package"
						smooth="true"
						className="text-white hover:text-blue-300 mx-4 block py-2 md:inline-block">
						Services
					</ScrollLink>
				</div>
				<div
					className={`md:col-start-11 md:col-span-4 flex md:flex-row justify-center items-center ${
						isOpen ? "block" : "hidden"
					} md:block`}>
					<RouterLink to="auth/register" className="text-white hover:text-blue-300 mx-4 block py-2 md:inline-block">
						Register
					</RouterLink>
					<RouterLink to="auth/login" className="text-white hover:text-blue-300 mx-4 block py-2 md:inline-block">
						Login
					</RouterLink>
				</div>
			</nav>
		</header>
	);
};

export default Header;
