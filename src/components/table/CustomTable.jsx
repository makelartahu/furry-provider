import PropTypes from "prop-types";
import * as React from "react";
import { Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from "@mui/material";

const CustomTable = ({ data }) => {
	if (!data || data.length === 0) {
		return <div>No data available</div>;
	}

	return (
		<TableContainer component={Paper}>
			<Table>
				<TableHead>
					<TableRow>
						{Object.keys(data[0]).map((key) => (
							<TableCell key={key} sx={{ backgroundColor: "slategray", color: "white", border: "1px solid grey" }}>
								{key}
							</TableCell>
						))}
					</TableRow>
				</TableHead>
				<TableBody>
					{data.map((row, index) => (
						<TableRow key={index}>
							{Object.values(row).map((value, i) => (
								<TableCell key={i} sx={{ border: "1px solid grey" }}>
									{value}
								</TableCell>
							))}
						</TableRow>
					))}
				</TableBody>
			</Table>
		</TableContainer>
	);
};

CustomTable.propTypes = {
	data: PropTypes.arrayOf(PropTypes.object),
};

export default CustomTable;
