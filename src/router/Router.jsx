import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import MainLayout from "../layout/mainLayout";
import AuthLayout from "../layout/authLayout";
import Home from "../pages/Home/Home";
import RegisterPage from "../pages/Register/Register";
import Order from "../pages/Order/Order";
import Verifikasi from "../pages/Verifikasi/Verifikasi";
import Pasang from "../pages/Pasang/Pasang";
import LoginPage from "../pages/Login/Login";
import ErrorPage from "../pages/Error/Errorpage";
import OrderLayout from "../layout/orderLayout";
import Selesai from "../pages/Selesai/Selesai";
import { useEffect, useState } from "react";
import axios from "axios";
import Admin from "../pages/Admin/Admin";

const AppRouter = () => {
	// useEffect(() => {
	// 	const fetchStatus = async () => {
	// 		const token = sessionStorage.getItem("token");
	// 		if (!token) {
	// 			return;
	// 		}

	// 		try {
	// 			const res = await axios.post(
	// 				"https://lp0xwz14-3000.asse.devtunnels.ms/api/order",
	// 				{},
	// 				{
	// 					headers: {
	// 						Authorization: `Bearer ${token}`,
	// 					},
	// 				},
	// 			);
	// 			if (res.data.message === "No order found for this user") {
	// 				sessionStorage.setItem("currentStep", 1);
	// 			} else {
	// 				const statusIdFromResponse = res.data.data.data.status_id;
	// 				sessionStorage.setItem("currentStep", statusIdFromResponse);
	// 			}
	// 		} catch (err) {
	// 			console.log(err);
	// 		}
	// 	};

	// 	fetchStatus();
	// }, []);
	return (
		<Router>
			<Routes>
				<Route path="/" element={<MainLayout />}>
					<Route index element={<Home />} />
				</Route>
				<Route path="auth/*" element={<AuthLayout />}>
					<Route path="register" element={<RegisterPage />} />
					<Route path="login" element={<LoginPage />} />
				</Route>
				<Route path="order" element={<OrderLayout />}>
					<Route index element={<Order />} />
					<Route path="verifikasi" element={<Verifikasi />} />
					<Route path="pasang" element={<Pasang />} />
					<Route path="selesai" element={<Selesai />} />
				</Route>
				<Route path="admin" element={<OrderLayout />}>
					<Route index element={<Admin />} />
				</Route>
				<Route path="*" element={<ErrorPage />} />
			</Routes>
		</Router>
	);
};

export default AppRouter;
