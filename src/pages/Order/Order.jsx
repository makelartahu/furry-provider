import ProgressBar from "../../components/progress-bar/ProgressBar";
import CustomTextField from "../../components/textfield/CustomTextField";
import { useEffect, useState, useRef } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import { TextField, Button } from "@mui/material";
import imageCompression from "browser-image-compression";

const Order = () => {
	const navigate = useNavigate();
	const [formData, setFormData] = useState({
		nama: "",
		email: "",
		upload_identitas: null,
		kota: "",
		kecamatan: "",
		jalan: "",
		paket_id: 0,
	});
	const [errors, setErrors] = useState({});
	const [currentStep, setCurrentStep] = useState(0);
	const [roleId, setRoleId] = useState(0);

	const fileInput = useRef(null);

	useEffect(() => {
		const fetchStatus = async () => {
			try {
				const token = localStorage.getItem("token");
				const res = await axios.post(
					"https://lp0xwz14-3000.asse.devtunnels.ms/api/order",
					{},
					{
						headers: {
							Authorization: `Bearer ${token}`,
						},
					},
				);
				if (res.data.message === "No order found for this user") {
					sessionStorage.setItem("currentStep", 1);
				} else {
					const statusIdFromResponse = res.data.data.data.status_id;
					sessionStorage.setItem("currentStep", statusIdFromResponse);
					setCurrentStep(Number(sessionStorage.getItem("currentStep")));
				}
			} catch (err) {
				console.log(err);
			}
		};
		fetchStatus();
	}, []);

	useEffect(() => {
		const fetchRole = async () => {
			try {
				const token = localStorage.getItem("token");
				const res = await axios.post(
					"https://lp0xwz14-3000.asse.devtunnels.ms/api/user",
					{},
					{
						headers: {
							Authorization: `Bearer ${token}`,
						},
					},
				);
				setRoleId(res.data.data.roleId);
			} catch (err) {
				console.log(err);
			}
		};
		fetchRole();
	}, []);

	useEffect(() => {
		if (roleId === 1) {
			navigate("/admin");
		}
	}, [roleId, navigate]);

	useEffect(() => {
		switch (currentStep) {
			case 1:
				navigate("/order");
				break;
			case 2:
			case 3:
			case 4:
				navigate("/order/verifikasi");
				break;
			case 5:
			case 6:
				navigate("/order/pasang");
				break;
			case 7:
				navigate("/order/selesai");
				break;

			default:
				navigate("/order");
				break;
		}
	}, [currentStep, navigate]);

	const handleChange = async (e) => {
		if (e.target.name === "image") {
			let file = e.target.files[0];
			if (!file) {
				setErrors((prevErrors) => ({ ...prevErrors, image: "This field is required." }));
				return;
			}
			if (!file.type.startsWith("image/")) {
				setErrors((prevErrors) => ({ ...prevErrors, image: "File must be an image." }));
				return;
			}

			const options = {
				maxSizeMB: 1, // (default: Number.POSITIVE_INFINITY)
				maxWidthOrHeight: 1920, // compressedFile will scale down by ratio to a point that width or height is smaller than maxWidthOrHeight (default: undefined)
				useWebWorker: true, // optional, use multi-thread web worker, fallback to run in main-thread (default: true)
			};

			try {
				const compressedFile = await imageCompression(file, options);
				let reader = new FileReader();
				reader.onloadend = () => {
					setFormData({ ...formData, [e.target.name]: reader.result });
				};
				reader.readAsDataURL(compressedFile);
			} catch (error) {
				console.log(error);
			}
		} else {
			setFormData({ ...formData, [e.target.name]: e.target.value });
		}
	};
	const validateForm = () => {
		let temp = {};
		temp.nama = formData.nama ? "" : "This field is required.";
		temp.email = formData.email ? "" : "This field is required.";
		temp.kota = formData.kota ? "" : "This field is required.";
		temp.kecamatan = formData.kecamatan ? "" : "This field is required.";
		temp.jalan = formData.jalan ? "" : "This field is required.";
		temp.email = /^[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[A-Za-z]+$/.test(formData.email) ? "" : "Email is not valid.";
		setErrors({ ...temp });
		return Object.values(temp).every((x) => x === "");
	};

	const handleSubmit = (e) => {
		e.preventDefault();
		if (validateForm()) {
			try {
				const token = localStorage.getItem("token");
				const data = new FormData();

				data.append("nama", formData.nama);
				data.append("email", formData.email);
				data.append("kota", formData.kota);
				data.append("kecamatan", formData.kecamatan);
				data.append("jalan", formData.jalan);
				data.append("upload_identitas", fileInput.current.files[0]);
				if (formData.paket_id !== undefined) {
					data.append("paket_id", formData.paket_id);
				}
				axios.post("https://lp0xwz14-3000.asse.devtunnels.ms/api/order/create", data, {
					headers: {
						Authorization: `Bearer ${token}`,
					},
				});
			} catch (err) {
				console.log(err);
			}
		}
	};
	return (
		<section className="min-h-screen p-4 flex flex-col gap-20">
			<ProgressBar currentStep={currentStep} />
			<section className="flex  justify-evenly items-center w-screen gap-4">
				<div className="bg-white rounded-lg shadow-md p-8 a min-w-[300px] md:min-w-[500px] ">
					<h2 className="text-center text-2xl font-bold mb-8">Data Diri Pelanggan</h2>
					<div className="mb-6">
						<CustomTextField
							label="Full  Nama"
							placeholder=" Nama"
							name="nama"
							type="text"
							getInput={handleChange}
							error={Boolean(errors.nama)}
							helperText={errors.nama}
							color={!errors.nama && "success"}
							focused={!errors.nama}
						/>
					</div>
					<div className="mb-6">
						<CustomTextField
							label="Email"
							placeholder="Email"
							name="email"
							type="email"
							getInput={handleChange}
							error={Boolean(errors.email)}
							helperText={errors.email}
							color={!errors.email && "success"}
							focused={!errors.email}
						/>
					</div>

					<div className="mb-6 flex items-center">
						<TextField
							fullWidth
							value={fileInput.current?.files[0]?.name || ""}
							placeholder="Upload_identitas"
							InputProps={{
								readOnly: true,
							}}
							className="mr-3 flex-grow"
						/>
						<input
							accept="image/*"
							type="file"
							name="image"
							style={{ display: "none" }}
							ref={fileInput}
							onChange={handleChange}
							id="contained-button-file"
						/>
						<label htmlFor="contained-button-file">
							<Button variant="contained" color="primary" component="span" style={{ fontSize: "15px", height: "55px" }}>
								Upload
							</Button>
						</label>
					</div>
					<div className="mb-6">
						<TextField
							select
							label="Select Paket"
							name="paket_id"
							onChange={handleChange}
							value={formData.paket_id}
							SelectProps={{
								native: true,
							}}
							helperText="Please select your paket">
							<option value={1}>Paket Basic</option>
							<option value={2}>Paket Medium</option>
							<option value={3}>Paket Advance</option>
						</TextField>
					</div>
				</div>
				<div className="bg-white rounded-lg shadow-md p-8 min-w-[300px] md:min-w-[500px] ">
					<h2 className="text-center text-2xl font-bold mb-8">Alamat Pemasangan</h2>

					<div className="mb-6">
						<CustomTextField
							label="Kota"
							placeholder="Kota"
							name="kota"
							type="text"
							getInput={handleChange}
							error={Boolean(errors.kota)}
							helperText={errors.kota}
							color={!errors.kota && "success"}
							focused={!errors.kota}
						/>
					</div>
					<div className="mb-6">
						<div>
							<CustomTextField
								label="Kecamatan"
								placeholder="Kecamatan"
								name="kecamatan"
								type="text"
								getInput={handleChange}
								error={Boolean(errors.kecamatan)}
								helperText={errors.kecamatan}
								color={!errors.kecamatan && "success"}
								focused={!errors.kecamatan}
							/>
						</div>
					</div>
					<div className="mb-6">
						<CustomTextField
							label="Jalan"
							placeholder="Jalan"
							name="jalan"
							type="text"
							getInput={handleChange}
							error={Boolean(errors.jalan)}
							helperText={errors.jalan}
							color={!errors.jalan && "success"}
							focused={!errors.jalan}
						/>
					</div>

					<button
						onClick={handleSubmit}
						className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-lg flex flex-grow w-full h-14 items-center justify-center">
						Submit Data
					</button>
				</div>
			</section>
		</section>
	);
};

export default Order;
