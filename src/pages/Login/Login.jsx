import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import CustomTextField from "../../components/textfield/CustomTextField";
import axios from "axios";

const LoginPage = () => {
	const navigate = useNavigate();
	const [formData, setFormData] = useState({
		username: "",
		password: "",
	});

	const [errors, setErrors] = useState({});

	const handleChange = (e) => {
		setFormData({ ...formData, [e.target.name]: e.target.value });
	};

	const validateForm = () => {
		let temp = {};
		temp.username = formData.username ? "" : "This field is required.";
		temp.password = formData.password ? "" : "This field is required.";
		setErrors({ ...temp });
		return Object.values(temp).every((x) => x === "");
	};

	const handleSubmit = async (e) => {
		e.preventDefault();
		if (validateForm()) {
			try {
				const res = await axios.post("https://lp0xwz14-3000.asse.devtunnels.ms/api/user/login", formData);
				if (res.data.code === 0) {
					localStorage.setItem("token", res.data.data.token);
					setFormData({ username: "", password: "" });
					navigate("/order/");
				} else {
					setErrors({ ...errors, username: res.data.message });
				}
			} catch (err) {
				console.log(err);
			}
		}
	};

	return (
		<div className="min-h-screen flex items-center justify-center w-full relative">
			<img src="/assets/svg/wave-haikei.svg" alt="" className="w-full" />
			<div className="bg-white rounded-lg shadow-md p-8 absolute min-w-[500px]">
				<h2 className="text-center text-2xl font-bold mb-8">Login</h2>
				<form onSubmit={handleSubmit} className="">
					<div className="mb-6">
						<CustomTextField
							label="Username"
							placeholder="Username"
							name="username"
							type="text"
							getInput={handleChange}
							error={Boolean(errors.username)}
							helperText={errors.username}
						/>
					</div>
					<div className="mb-6">
						<CustomTextField
							label="Password"
							placeholder="Password"
							name="password"
							type="password"
							getInput={handleChange}
							error={Boolean(errors.password)}
							helperText={errors.password}
						/>
					</div>
					<button
						type="submit"
						className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-lg flex flex-grow w-full h-14 items-center justify-center">
						Login
					</button>
				</form>
			</div>
		</div>
	);
};

export default LoginPage;
