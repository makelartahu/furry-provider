import ProgressBar from "../../components/progress-bar/ProgressBar";
import CustomTextField from "../../components/textfield/CustomTextField";
import { useState, useEffect } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";

const Pasang = () => {
	const currentStep = Number(sessionStorage.getItem("currentStep"));
	const navigate = useNavigate();
	const [formData, setFormData] = useState({
		nama: "",
		email: "",
		upload_identitas: null,
		kota: "",
		kecamatan: "",
		jalan: "",
		paket_id: "",
		teknisi: {
			nama_teknisi: "",
			nomor_telepon: "",
			nip: "",
		},
	});

	useEffect(() => {
		switch (currentStep) {
			case 1:
				navigate("/order");
				break;
			case 2:
				navigate("/order/verifikasi");
				break;
			case 3:
			case 4:
				navigate("/order/verifikasi");

				break;
			case 5:
			case 6:
				navigate("/order/pasang");

				break;
			case 7:
				navigate("/order/selesai");

				break;

			default:
				break;
		}
	}, [currentStep, navigate]);

	useEffect(() => {
		const fetchStatus = async () => {
			try {
				const token = localStorage.getItem("token");
				const result = await axios.post(
					"https://lp0xwz14-3000.asse.devtunnels.ms/api/order",
					{},
					{
						headers: {
							Authorization: `Bearer ${token}`,
						},
					},
				);
				if (result.data.data.data) {
					setFormData((prevFormData) => ({ ...prevFormData, ...result.data.data.data }));
				}
			} catch (err) {
				console.log(err);
			}
		};
		fetchStatus();
	}, []);

	return (
		<section className="min-h-screen p-4 flex flex-col gap-20">
			<ProgressBar currentStep={currentStep} />
			<section className="flex  justify-evenly items-center w-screen gap-4">
				<div className="bg-white rounded-lg shadow-md p-8 a min-w-[300px] md:min-w-[500px] ">
					<h2 className="text-center text-2xl font-bold mb-8">Data teknisi</h2>

					<div className="mb-6">
						<CustomTextField
							disabled={true}
							value={formData.teknisi.nama_teknisi}
							label="teknisi"
							placeholder="teknsi"
							name="teknisi.nama_teknisi"
							type="text"
						/>
					</div>
					<div className="mb-6">
						<CustomTextField
							disabled={true}
							value={formData.teknisi.nomor_telepon}
							label="Nomor Telepon"
							placeholder="Nomor Telepon"
							name="teknisi.nomor_telepon"
							type="phone"
						/>
					</div>
					<div className="mb-6">
						<CustomTextField
							disabled={true}
							value={formData.teknisi.nip}
							label="NIP"
							placeholder="NIP"
							name="teknisi.nip"
							type="number"
						/>
					</div>
				</div>
				<div className="bg-white rounded-lg shadow-md p-8 min-w-[300px] md:min-w-[500px] ">
					<h2 className="text-center text-2xl font-bold mb-8">Alamat Pemasangan</h2>

					<div className="mb-6">
						<CustomTextField
							disabled={true}
							value={formData.kota}
							label="Kota"
							placeholder="Kota"
							name="Kota"
							type="text"
						/>
					</div>
					<div className="mb-6">
						<div>
							<CustomTextField
								disabled={true}
								value={formData.kecamatan}
								label="Kecamatan"
								placeholder="Kecamatan"
								name="kecamatan"
								type="text"
							/>
						</div>
					</div>
					<div className="mb-6">
						<CustomTextField
							disabled={true}
							value={formData.jalan}
							label="Jalan"
							placeholder="Jalan"
							name="jalan"
							type="text"
						/>
					</div>
				</div>
			</section>
		</section>
	);
};

export default Pasang;
