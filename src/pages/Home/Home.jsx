import Button from "@mui/material/Button";
import RouterIcon from "@mui/icons-material/Router";
import WifiIcon from "@mui/icons-material/Wifi";
import MoneyOffCsredIcon from "@mui/icons-material/MoneyOffCsred";
import CustomCard from "../../components/card/CustomCard";
import PackageCard from "../../components/card/PackageCard";

const Home = () => {
	return (
		<>
			{/* First Page */}
			<section id="home">
				<div className="relative h-screen">
					<div
						className="absolute inset-0 bg-cover bg-center "
						style={{ backgroundImage: `url(/assets/image/img3.jpg)` }}></div>
					<div className="relative z-10 flex flex-col items-start justify-center h-full mx-4 md:mx-32 font-sans text-start md:text-left">
						<h1 className="text-6xl font-extrabold mb-4 text-slate-300 bg-black bg-opacity-10">
							Get Pure Fiber Broadband
						</h1>
						<p className="text-xl font-extrabold mb-8 text-[#37b1e4] w-[300px] md:w-[400px] text-shadow">
							Introducing the fastest internet in the world. Get ready for the future with our ultra-fast fiber optic
							broadband.
						</p>
						<Button variant="contained" style={{ width: "200px", height: "50px", fontSize: "20px" }}>
							Discover
						</Button>
					</div>
				</div>
			</section>
			{/* Second Page */}
			<section id="about">
				<div className="flex flex-col lg:flex-row items-center justify-center gap-8 md:gap-24 font-sans mt-12 p-4">
					<CustomCard
						text="Powerfull Connection 5G"
						textColor="black"
						IconComponent={RouterIcon}
						iconSize={70}
						flexDirection="row"
						size={{ height: "250px", width: "400px" }}
						shadow="lg"
						elevation="elevation-2"
					/>
					<CustomCard
						text="Powerfull Connection 5G"
						textColor="black"
						IconComponent={WifiIcon}
						iconSize={70}
						flexDirection="row"
						size={{ height: "250px", width: "400px" }}
						shadow="lg"
						elevation="elevation-2"
					/>
					<CustomCard
						text="No Additional Charges"
						textColor="black"
						IconComponent={MoneyOffCsredIcon}
						iconSize={70}
						flexDirection="row"
						size={{ height: "250px", width: "400px" }}
						shadow="lg"
						elevation="elevation-2"
					/>
				</div>

				<div className="flex flex-col lg:flex-row justify-center items-center mt-24 mb-12 gap-8 lg:mx-auto lg:w-3/4">
					<div className="z-10">
						<img src="/assets/image/img4.jpg" alt="image1" className="w-[400px] h-[500px] rounded-2xl" />
					</div>

					<div className="font-sans text-start w-3/4 flex items-start justify-start flex-col">
						<div className="mb-4">
							<small className="uppercase font-bold tracking-wide text-xl text-[#37b1e4]">ABOUT US</small>
						</div>
						<div className="mb-8">
							<h1 className="text-3xl md:text-4xl lg:text-6xl font-bold w-[200px] md:w-[500px] lg:-[700px]">
								Providing 5G Internet to support your needs
							</h1>
						</div>
						<div className="mb-4 md:w-[600px]">
							<span className="text-[#37b1e4] text-xl font-bold">
								Introducing 5G internet to support your needs. Get ready for the future with our ultra-fast fiber optic
							</span>
						</div>
						<div className="mb-8">
							<span className="text-gray-600 text-xl">
								We are the leading provider of 5G internet in the world. We provide the fastest internet in the world.
							</span>
						</div>
						<div>
							<ul className="list-disc list-inside text-xl">
								<li className="text-gray-600">Fastest Internet</li>
								<li className="text-gray-600">Unlimited Data</li>
								<li className="text-gray-600">24/7 Support</li>
							</ul>
						</div>
					</div>
				</div>
			</section>
			{/* Third Page */}

			<section id="package">
				<div className="h-auto bg-slate-300 py-12">
					<div className="flex flex-col gap-8 text-center font-sans">
						<h2 className="text-4xl font-bold">Chose Your Package</h2>
						<p>Chose a plat that`s right for your need. Simple pricing. No hidden charges.</p>
					</div>

					<div className="flex flex-col lg:flex-row justify-center items-center font-sans gap-8 md:gap-20 px-4 mt-8">
						<PackageCard title="BASIC" price="300.000" speed="300 Mb/s" devices="Up to 5 Devices" />
						<PackageCard title="Medium" price="400.000" speed="400 Mb/s" devices="Up to 10 Devices" />
						<PackageCard title="Advance" price="700.000" speed="300 Mb/s" devices="Up to 30 Devices" />
					</div>
				</div>
			</section>
		</>
	);
};

export default Home;
