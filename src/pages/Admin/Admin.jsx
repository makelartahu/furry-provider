import CustomTable from "../../components/table/CustomTable";
import { useEffect, useState } from "react";
import CustomTextField from "../../components/textfield/CustomTextField";
import ProgressBar from "../../components/progress-bar/ProgressBar";
import { TextField, Button } from "@mui/material";
import axios from "axios";
const Admin = () => {
	const [paketData, setPaketData] = useState([
		{
			ID: 1,
			"NAMA PAKET": "",
			"JUMLAH PENJUALAN": 0,
		},
	]);
	const [teknisiData, setTeknisiData] = useState([
		{
			ID: 1,
			"NAMA TEKNISI": "",
			"TOTAL HANDLING": 0,
		},
	]);

	const [orderData, setOrderData] = useState([
		{
			ID: 1,
			email: "",
			nama_paket: "",
		},
	]);

	const [formData, setFormData] = useState({
		nama: "",
		email: "",
		kota: "",
		kecamatan: "",
		jalan: "",
		image: null,
	});

	useEffect(() => {
		const fetchPaket = async () => {
			try {
				const res = await axios.post("https://lp0xwz14-3000.asse.devtunnels.ms/api/paket", {
					headers: {
						Authorization: `Bearer ${localStorage.getItem("token")}`,
					},
				});
				setPaketData(res.data.data);
			} catch (err) {
				console.log(err);
			}
		};

		const fetchTeknisi = async () => {
			try {
				const res = await axios.post("https://lp0xwz14-3000.asse.devtunnels.ms/api/teknisi/list", {
					headers: {
						Authorization: `Bearer ${localStorage.getItem("token")}`,
					},
				});
				setTeknisiData(res.data.data);
			} catch (err) {
				console.log(err);
			}
		};

		const fetchOrder = async () => {
			try {
				const res = await axios.post(
					"https://lp0xwz14-3000.asse.devtunnels.ms/api/order/list",
					{},
					{
						headers: {
							Authorization: `Bearer ${localStorage.getItem("token")}`,
						},
					},
				);
				setOrderData(res.data.data.data.map(({ email, paket }) => ({ email, paket })));
				setFormData((prevFormData) => ({ ...prevFormData, ...res.data.data.data }));
			} catch (err) {
				console.log(err);
			}
		};

		fetchPaket();
		fetchTeknisi();
		fetchOrder();
	}, []);

	const handleChange = (e) => {};

	return (
		<div className="min-h-screen p-4 flex flex-col gap-28">
			{/* First Page */}

			<section className="flex justify-evenly items-start">
				<div className="flex flex-col justify-between items-start gap-4 mt-44">
					<h1 className="text-2xl font-extrabold">Data Pesanan</h1>
					<div className="min-w-[700px]">
						<CustomTable
							data={orderData.map(({ email, paket }) => ({
								Email: email,
								"Nama Paket": paket ? paket.nama_paket : "N/A",
							}))}
						/>
					</div>
				</div>
				<div className="flex flex-col justify-between items-start gap-12 mt-40  ">
					<section>
						<h1 className="text-2xl font-extrabold">Data Paket Terbanyak Terjual</h1>
						<div className="min-w-[700px] mt-8">
							<CustomTable
								data={paketData
									.map(({ nama_paket, jumlah_penjualan }) => ({
										"Nama Paket": nama_paket,
										"Jumlah Penjualan": jumlah_penjualan,
									}))
									.sort((a, b) => b["Jumlah Penjualan"] - a["Jumlah Penjualan"])}
							/>
						</div>
					</section>
					<section>
						<h1 className="text-2xl font-extrabold">Data Teknisi Terbanyak Ditugaskan</h1>
						<div className="min-w-[700px] mt-8">
							<CustomTable
								data={teknisiData
									.map(({ nama_teknisi, total_handling }) => ({
										"Nama Teknisi": nama_teknisi,
										"Jumlah Tugas": total_handling,
									}))
									.sort((a, b) => b["Jumlah Tugas"] - a["Jumlah Tugas"])}
							/>
						</div>
					</section>
				</div>
			</section>
			<hr />
			{/* Second Page */}

			<section className="min-h-screen p-4 flex flex-col gap-20">
				<ProgressBar currentStep={2} />
				<section className="flex  justify-evenly items-center w-screen gap-4">
					<div className="bg-white rounded-lg shadow-md p-8 a min-w-[300px] md:min-w-[500px] ">
						<h2 className="text-center text-2xl font-bold mb-8">Data Diri Pelanggan</h2>
						<div className="mb-6">
							<CustomTextField disabled={true} label="Full  Nama" placeholder=" Nama" name="nama" type="text" />
						</div>
						<div className="mb-6">
							<CustomTextField disabled={true} label="Email" placeholder="Email" name="email" type="email" />
						</div>

						<div className="mb-6 flex items-center">
							<TextField
								fullWidth
								value={formData.image || ""}
								placeholder="Upload_identitas"
								InputProps={{
									readOnly: true,
								}}
								className="mr-3 flex-grow"
							/>
						</div>
					</div>
					<div className="bg-white rounded-lg shadow-md p-8 min-w-[300px] md:min-w-[500px] ">
						<h2 className="text-center text-2xl font-bold mb-8">Alamat Pemasangan</h2>

						<div className="mb-6">
							<CustomTextField disabled={true} label="Kota" placeholder="Kota" name="Kota" type="text" />
						</div>
						<div className="mb-6">
							<div>
								<CustomTextField
									disabled={true}
									label="Kecamatan"
									placeholder="Kecamatan"
									name="kecamatan"
									type="text"
								/>
							</div>
						</div>
						<div className="mb-6">
							<CustomTextField disabled={true} label="Jalan" placeholder="Jalan" name="jalan" type="text" />
						</div>
					</div>
				</section>
			</section>
		</div>
	);
};

export default Admin;
