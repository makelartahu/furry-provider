import Progress from "../../components/progress-bar/ProgressBar";
import { useEffect } from "react";
import { useNavigate } from "react-router-dom";

const Selesai = () => {
	const currentStep = Number(sessionStorage.getItem("currentStep"));
	const navigate = useNavigate();

	useEffect(() => {
		switch (currentStep) {
			case 1:
				navigate("/order");
				break;
			case 2:
				navigate("/order/verifikasi");
				break;
			case 3:
			case 4:
				navigate("/order/pasang");
				break;
			case 5:
			case 6:
				navigate("/order/pasang");
				break;
			case 7:
				navigate("/order/selesai");
				break;
			default:
				break;
		}
	}, [currentStep, navigate]);

	return (
		<div className="min-h-screen p-4">
			<Progress currentStep={currentStep} />

			<div className="flex justify-center items-center flex-col mt-20 gap-4">
				<h1 className="text-2xl font-extrabold">Pemasangan Selesai</h1>
				<h2 className="text-2xl font-extrabold">Silakan Lakukan Pembayaran Untuk Mengaktifkan Layanan Internet anda</h2>
			</div>
		</div>
	);
};

export default Selesai;
