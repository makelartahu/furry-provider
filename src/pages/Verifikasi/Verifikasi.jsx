import ProgressBar from "../../components/progress-bar/ProgressBar";
import CustomTextField from "../../components/textfield/CustomTextField";
import { useState, useEffect, useRef } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import { TextField, Button } from "@mui/material";
import imageCompression from "browser-image-compression";

import PropTypes from "prop-types";

const Verifikasi = () => {
	const currentStep = Number(sessionStorage.getItem("currentStep"));
	const navigate = useNavigate();
	const [isValid, setIsValid] = useState(true);
	const [formData, setFormData] = useState({
		nama: "",
		email: "",
		upload_identitas: null,
		kota: "",
		kecamatan: "",
		jalan: "",
		paket_id: "",
		reject_reason: "",
	});
	const [errors, setErrors] = useState({});

	const fileInput = useRef(null);

	useEffect(() => {
		const fetchStatus = async () => {
			try {
				const token = localStorage.getItem("token");
				const result = await axios.post(
					"https://lp0xwz14-3000.asse.devtunnels.ms/api/order",
					{},
					{
						headers: {
							Authorization: `Bearer ${token}`,
						},
					},
				);
				if (result.data.data.data) {
					setFormData((prevFormData) => ({ ...prevFormData, ...result.data.data.data }));
				}
			} catch (err) {
				console.log(err);
			}
		};
		fetchStatus();
	}, []);

	useEffect(() => {
		switch (currentStep) {
			case 1:
				navigate("/order");
				setIsValid(true);
				break;
			case 2:
				setIsValid(true);
				navigate("/order/verifikasi");
				break;
			case 3:
			case 4:
				navigate("/order/verifikasi");
				setIsValid(false);
				break;
			case 5:
			case 6:
				navigate("/order/pasang");
				setIsValid(true);
				break;
			case 7:
				navigate("/order/selesai");
				setIsValid(true);
				break;

			default:
				break;
		}
	}, [currentStep, navigate]);

	const handleChange = async (e) => {
		if (e.target.name === "image") {
			let file = e.target.files[0];
			if (!file) {
				setErrors((prevErrors) => ({ ...prevErrors, image: "This field is required." }));
				return;
			}
			if (!file.type.startsWith("image/")) {
				setErrors((prevErrors) => ({ ...prevErrors, image: "File must be an image." }));
				return;
			}

			const options = {
				maxSizeMB: 1, // (default: Number.POSITIVE_INFINITY)
				maxWidthOrHeight: 1920, // compressedFile will scale down by ratio to a point that width or height is smaller than maxWidthOrHeight (default: undefined)
				useWebWorker: true, // optional, use multi-thread web worker, fallback to run in main-thread (default: true)
			};

			try {
				const compressedFile = await imageCompression(file, options);
				let reader = new FileReader();
				reader.onloadend = () => {
					setFormData({ ...formData, [e.target.name]: reader.result });
				};
				reader.readAsDataURL(compressedFile);
			} catch (error) {
				console.log(error);
			}
		} else {
			setFormData({ ...formData, [e.target.name]: e.target.value });
		}
	};
	const validateForm = () => {
		let temp = {};
		temp.nama = formData.nama ? "" : "This field is required.";
		temp.email = formData.email ? "" : "This field is required.";
		temp.kota = formData.kota ? "" : "This field is required.";
		temp.kecamatan = formData.kecamatan ? "" : "This field is required.";
		temp.jalan = formData.jalan ? "" : "This field is required.";
		temp.email = /^[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[A-Za-z]+$/.test(formData.email) ? "" : "Email is not valid.";
		setErrors({ ...temp });
		return Object.values(temp).every((x) => x === "");
	};

	const handleSubmit = (e) => {
		e.preventDefault();
		if (validateForm()) {
			try {
				const token = localStorage.getItem("token");
				const data = new FormData();

				data.append("nama", formData.nama);
				data.append("email", formData.email);
				data.append("kota", formData.kota);
				data.append("kecamatan", formData.kecamatan);
				data.append("jalan", formData.jalan);
				data.append("upload_identitas", fileInput.current.files[0]);
				axios.post("https://lp0xwz14-3000.asse.devtunnels.ms/api/order/create", data, {
					headers: {
						Authorization: `Bearer ${token}`,
					},
				});
			} catch (err) {
				console.log(err);
			}
		}
	};

	return (
		<section className="min-h-screen p-4 flex flex-col gap-20">
			<ProgressBar currentStep={currentStep} />
			<section className="flex  justify-evenly items-center w-screen gap-4">
				<div className="bg-white rounded-lg shadow-md p-8 a min-w-[300px] md:min-w-[500px] ">
					<h2 className="text-center text-2xl font-bold mb-8">Data Diri Pelanggan</h2>
					<div className="mb-6">
						<CustomTextField
							disabled={isValid}
							label="Full  Nama"
							placeholder=" Nama"
							name="nama"
							type="text"
							getInput={handleChange}
							error={Boolean(errors.nama)}
							helperText={errors.nama}
							value={formData.nama}
						/>
					</div>
					<div className="mb-6">
						<CustomTextField
							disabled={isValid}
							label="Email"
							placeholder="Email"
							name="email"
							type="email"
							getInput={handleChange}
							error={Boolean(errors.email)}
							helperText={errors.email}
							value={formData.email}
						/>
					</div>

					<div className="mb-6 flex items-center">
						<TextField
							fullWidth
							value={fileInput.current?.files[0]?.name || ""}
							placeholder="Upload_identitas"
							InputProps={{
								readOnly: true,
							}}
							className="mr-3 flex-grow"
						/>
						<input
							accept="image/*"
							type="file"
							name="image"
							style={{ display: "none" }}
							ref={fileInput}
							onChange={handleChange}
							id="contained-button-file"
						/>
						<label htmlFor="contained-button-file">
							{!isValid && (
								<Button
									variant="contained"
									color="primary"
									component="span"
									style={{ fontSize: "15px", height: "55px" }}>
									Upload
								</Button>
							)}
						</label>
					</div>
				</div>
				<div className="bg-white rounded-lg shadow-md p-8 min-w-[300px] md:min-w-[500px] ">
					<h2 className="text-center text-2xl font-bold mb-8">Alamat Pemasangan</h2>

					<div className="mb-6">
						<CustomTextField
							disabled={isValid}
							label="Kota"
							placeholder="Kota"
							name="Kota"
							type="text"
							getInput={handleChange}
							error={Boolean(errors.kota)}
							helperText={errors.kota}
							value={formData.kota}
						/>
					</div>
					<div className="mb-6">
						<div>
							<CustomTextField
								disabled={isValid}
								label="Kecamatan"
								placeholder="Kecamatan"
								name="kecamatan"
								type="text"
								getInput={handleChange}
								error={Boolean(errors.kecamatan)}
								helperText={errors.kecamatan}
								value={formData.kecamatan}
							/>
						</div>
					</div>
					<div className="mb-6">
						<CustomTextField
							disabled={isValid}
							label="Jalan"
							placeholder="Jalan"
							name="jalan"
							type="text"
							getInput={handleChange}
							error={Boolean(errors.jalan)}
							helperText={errors.jalan}
							value={formData.jalan}
						/>
					</div>

					{!isValid && (
						<button
							onClick={handleSubmit}
							className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-lg flex flex-grow w-full h-14 items-center justify-center">
							Perbaiki Data
						</button>
					)}
				</div>
			</section>
		</section>
	);
};

Verifikasi.propTypes = {
	currentStep: PropTypes.number,
};

export default Verifikasi;
