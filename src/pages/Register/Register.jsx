import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import CustomTextField from "../../components/textfield/CustomTextField";
import axios from "axios";

const RegisterPage = () => {
	const navigate = useNavigate();
	const [formData, setFormData] = useState({
		nama: "",
		username: "",
		password: "",
	});

	const [errors, setErrors] = useState({});

	const handleChange = (e) => {
		setFormData({ ...formData, [e.target.name]: e.target.value });
	};

	const validateForm = () => {
		let temp = {};
		temp.nama = formData.nama ? "" : "This field is required.";
		temp.username = formData.username ? "" : "This field is required.";
		temp.password = formData.password ? "" : "This field is required.";
		temp.confirmPassword = formData.confirmPassword ? "" : "This field is required.";
		temp.confirmPassword = formData.password === formData.confirmPassword ? "" : "Password does not match.";
		temp.username = /^[a-zA-Z0-9]+$/.test(formData.username) ? "" : "Username is not valid.";
		temp.username = formData.username.length > 6 ? "" : "Username must be at most 6 characters.";
		temp.username = formData.username.length < 20 ? "" : "Username must be at least 20 characters.";
		temp.password = formData.password.length > 8 ? "" : "Password must be at least 8 characters.";
		setErrors({ ...temp });
		return Object.values(temp).every((x) => x === "");
	};

	const handleSubmit = async (e) => {
		e.preventDefault();
		if (validateForm()) {
			try {
				const res = await axios.post("https://lp0xwz14-3000.asse.devtunnels.ms/api/user/register", formData);
				if (res.data.code === 0) {
					// Check if code is 0
					setFormData({ nama: "", username: "", password: "" }); // Reset form data
					navigate("/auth/login"); // Navigate to login
				} else {
					setErrors({ ...errors, username: res.data.message });
				}
			} catch (err) {
				console.log(err);
			}
		}
	};

	const isMobile = window.innerWidth <= 768;

	return (
		<div className="flex items-center justify-center w-full relative">
			{isMobile ? (
				<img src="/assets/svg/wave-haikei-vertical.svg" alt="" className="w-full h-full" />
			) : (
				<img src="/assets/svg/wave-haikei.svg" alt="" className="w-full" />
			)}
			<div className="bg-white rounded-lg shadow-md p-8 absolute min-w-[300px] md:min-w-[500px]">
				<h2 className="text-center text-2xl font-bold mb-8">Register</h2>
				<form onSubmit={handleSubmit} className="">
					<div className="mb-6">
						<CustomTextField
							label="Full Name"
							placeholder="Name"
							name="nama"
							type="text"
							getInput={handleChange}
							error={Boolean(errors.nama)}
							helperText={errors.nama}
						/>
					</div>
					<div className="mb-6">
						<div>
							<CustomTextField
								label="Username"
								placeholder="Username"
								name="username"
								type="text"
								getInput={handleChange}
								error={Boolean(errors.username)}
								helperText={errors.username}
							/>
						</div>
					</div>
					<div className="mb-6">
						<CustomTextField
							label="Password"
							placeholder="Password"
							name="password"
							type="password"
							getInput={handleChange}
							error={Boolean(errors.password)}
							helperText={errors.password}
						/>
					</div>
					<div className="mb-6">
						<CustomTextField
							label="Confirm Password"
							placeholder="Confirm Password"
							name="confirmPassword"
							type="password"
							getInput={handleChange}
							error={Boolean(errors.confirmPassword)}
							helperText={errors.confirmPassword}
						/>
					</div>
					<button
						type="submit"
						className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-lg flex flex-grow w-full h-14 items-center justify-center">
						Register
					</button>
				</form>
			</div>
		</div>
	);
};

export default RegisterPage;
