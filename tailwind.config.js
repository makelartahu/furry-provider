export default {
	content: ["./src/**/*.{js,jsx,ts,tsx}"],
	theme: {
		extend: {
			fontFamily: {
				sans: ["Roboto", "sans-serif"],
			},
			textShadow: {
				default: "0 2px 5px rgba(0, 0, 0, 0.5)",
			},
		},
	},
	plugins: [],
};
